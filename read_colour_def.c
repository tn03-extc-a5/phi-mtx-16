int readcolour()
{

  digitalWrite(S2, LOW);
  digitalWrite(S3, LOW);

  frequency = pulseIn(sensorOut, LOW);
  int R = frequency;
  //for red
  Serial.print("Red= ");
  Serial.print(frequency);
  Serial.print("  ");
  delay(50);
  //for green
  digitalWrite(S2, HIGH);
  digitalWrite(S3, HIGH);

  frequency = pulseIn(sensorOut, LOW);
  int G = frequency;

  Serial.print("Green= ");
  Serial.print(frequency);
  Serial.print("  ");
  delay(50);
  //for blue
  digitalWrite(S2, LOW);
  digitalWrite(S3, HIGH);

  frequency = pulseIn(sensorOut, LOW);
  int B = frequency;

  Serial.print("Blue= ");
  Serial.print(frequency);
  Serial.println("  ");
  delay(50);
  if(R<45 & R>30 & G<65 & G>55)
    {
    colour = 1; // Red
    }
  if(R<55 & R>40 & G<55 & G>40)
    {
    colour = 2; // Green
    }
  if (G<55 & G>45 & B<40 &B>25)
    {
    colour = 3; // Blue
    }
  return colour;
}
