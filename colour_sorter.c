#include <stdio.h>
#include <Servo.h>
#include "read_colour.h"

#define S0 2
#define S1 3
#define S2 4
#define S3 5
#define sensorOut 6

Servo topservo;
Servo bottomservo;

int frequency = 0;
int color=0;

void setup() {
  
  pinMode(S0, OUTPUT);
  pinMode(S1, OUTPUT);
  pinMode(S2, OUTPUT);
  pinMode(S3, OUTPUT);
  pinMode(sensorOut, INPUT);
  
  //Setting frequency-scaling to 20%
  digitalWrite(S0, HIGH);
  digitalWrite(S1, LOW);
  topservo.attach(7);
  bottomservo.attach(8);
  Serial.begin(9600);
}
void loop() 

{
  topServo.write(100);
  delay(100);
  
  for(int i = 100; i > 60; i--) {
    topServo.write(i);
    delay(2);
  }
  delay(100);
  
  colour = colour();
 
 delay(10);  
  switch (colour) {
    case 1:
    bottomServo.write(50);
    break;
    case 2:
    bottomServo.write(70);
    break;
    case 3:
    bottomServo.write(90);
    break;
	case 0:
    break;
  }
 
    delay(300);
  
  for(int i = 60; i > 30; i--) 
  {
    topservo.write(i);
    delay(2);
  } 
  delay(200);
  
  for(int i = 30; i < 100; i++)
  {
    topservo.write(i);
    delay(2);
  }
  colour=0;
}